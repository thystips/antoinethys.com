<?php

namespace App\Http\Controllers;

use App;

class CvController extends Controller
{
    public function index(){
        $contact = App\Contact::firstOrFail();
        /*$categories = App\Categorie::all();*/
        $categories = App\CategorieSlave::all();
        return view('cv', [
            'contact' => $contact,
            'social' => $contact->socials,
            'competences_dev' => $contact->competences->where('Categorie', '=', 'DEV'),
            'competences_infra' => $contact->competences->where('Categorie', '=', 'INFRA'),
            'experiences' => $contact->experiences,
            'portfolio_cat' => $categories,
            'portfolio' => $contact->projets
        ]);
    }
}
