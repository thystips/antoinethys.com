<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendEmailController extends Controller
{
    function send(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required',
            'last'     =>  'required',
            'email'  =>  'required|email',
            'subject'  =>  'required',
            'message' =>  'required'
        ]);

        $data = array(
            'name'      =>  $request->name,
            'last'      =>  $request->last,
            'email'      =>  $request->email,
            'subject'      =>  $request->subject,
            'message'   =>   $request->message
        );

        Mail::to('contact@antoinethys.com')->send(new SendMail($data));
        return back()->with('success', 'Merci de m\'avoir contacté');

    }
}
