<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = 'portfolios';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    protected $appends = ['categorie'];
    protected $with = ['categories'];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Categorie', 'categorie_portfolio', 'categorie_id', 'portfolio_id');
    }

    public function categorie_slave()
    {
        return $this->belongsTo('App\CategorieSlave');
    }

    public function getCategorie()
    {
        $categorie = $this->categories()->get();
        return $categorie;
    }
}
