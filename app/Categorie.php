<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public function projets()
    {
        return $this->belongsToMany('App\Portfolio', 'categorie_portfolio', 'portfolio_id', 'categorie_id');
    }

}
