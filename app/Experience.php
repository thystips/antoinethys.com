<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $table = 'experiences';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }
}
