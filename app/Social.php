<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $table = 'socials';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }
}
