<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class Contact extends Model
{
    use Notifiable;

    protected $table = 'contacts';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    protected $dates = [
        'created_at',
        'updated_at',
        'BirthDate'
    ];

    public function competences()
    {
        return $this->hasMany('App\Competence');
    }

    public function experiences()
    {
        return $this->hasMany('App\Experience');
    }

    public function projets()
    {
        return $this->hasMany('App\Portfolio');
    }

    public function socials()
    {
        return $this->hasMany('App\Social');
    }

    public function getAge()
    {
        return Carbon::parse($this->attributes['BirthDate'])->age;
    }

}
