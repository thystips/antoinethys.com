<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategorieSlave extends Model
{
    protected $table = 'categorie_slave';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public function portfolios()
    {
        return $this->hasMany('App\Portfolio');
    }
}
