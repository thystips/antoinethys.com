<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competence extends Model
{
    protected $table = 'competences';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }
}
