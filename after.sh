#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.
#
# If you have user-specific configurations you would like
# to apply, you may also create user-customizations.sh,
# which will be run after this script.

# If you're not quite ready for Node 12.x
# Uncomment these lines to roll back to
# v11.x or v10.x

# Remove Node.js v12.x:
#sudo apt-get -y purge nodejs
#sudo rm -rf /usr/lib/node_modules/npm/lib
#sudo rm -rf //etc/apt/sources.list.d/nodesource.list

# Install Node.js v11.x
#curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
#sudo apt-get install -y nodejs

# Install Node.js v10.x
#curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
#sudo apt-get install -y nodejs

# Install Xdebug
#git clone git://github.com/xdebug/xdebug.git
#cd xdebug
#phpize
#./configure --enable-xdebug
#make
#make install

# Configure Xdebug
#cat > /etc/php/7.3/mods-available/xdebug.ini <<EOL
sudo tee -a /etc/php/7.3/mods-available/xdebug.ini <<EOL
zend_extension=xdebug.so
xdebug.default_enable=1
xdebug.remote_enable=1
xdebug.remote_port=9000
xdebug.remote_autostart=1
xdebug.remote_connect_back=1
xdebug.remote_log = /tmp/xdebug.log
xdebug.remote_host=192.168.10.1
EOL
sudo service php7.3-fpm restart
sudo phpenmod xdebug
sudo service nginx restart

