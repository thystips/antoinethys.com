<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriePortfolioPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorie_portfolio', function (Blueprint $table) {
            $table->foreignId('categorie_id')->constrained('categories');
            $table->foreignId('portfolio_id')->constrained('portfolios');
            $table->primary(['categorie_id', 'portfolio_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorie_portfolio');
    }
}
