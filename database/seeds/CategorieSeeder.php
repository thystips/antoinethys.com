<?php

use Illuminate\Database\Seeder;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id' => 1,
            'Name' => 'Logiciel',
            'Class' => 'log',
        ]);
        DB::table('categories')->insert([
            'id' => 2,
            'Name' => 'Infra / SI',
            'Class' => 'infra',
        ]);
        DB::table('categories')->insert([
            'id' => 3,
            'Name' => 'Web',
            'Class' => 'web',
        ]);
        DB::table('categories')->insert([
            'id' => 4,
            'Name' => 'Stages',
            'Class' => 'stages',
        ]);
    }
}
