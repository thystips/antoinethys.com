<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('img/fav-icon.png') }}" type="image/x-icon" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>antoinethys.com</title>

    <!-- Icon css link -->
    <link href="{{ asset('vendors/material-icon/css/materialdesignicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/linears-icon/style.css') }}" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="{{ asset('vendors/owl-carousel/assets/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/animate-css/animate.css') }}" rel="stylesheet">

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">

    <link href="{{ asset('css/color.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="dark_bg" data-spy="scroll" data-target="#bs-example-navbar-collapse-1" data-offset="80" data-scroll-animation="true">

<div id="preloader">
    <div id="preloader_spinner">
        <div class="spinner"></div>
    </div>
</div>

<!--================ Frist header Area =================-->
<header class="header_area">
    <div class="container">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Boutons de navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" alt="Logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#about">à propos</a></li>
                    <li><a href="#skill">compétences</a></li>
                    <li><a href="#education">parcours</a></li>
                    <!-- <li><a href="#service">SERVICES</a></li> -->
                    <li><a href="#portfolio">portfolio</a></li>
                    <li><a href="#contact">contact</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
</header>
<!--================End Footer Area =================-->

<!--================Total container Area =================-->
@yield('cv')
<!--================End Total container Area =================-->

<!--================footer Area =================-->
<footer class="footer_area">
    <div class="footer_inner">
        <div class="container">
            <img id="footer-logo" src="{{ asset('img/footer-logo.png') }}" alt="">
            <ul class="social_icon">
                @foreach ($social as $social_footer)
                <li style="">
                    <a href="https://{{ $social_footer['Link'] }}"><i class="fa fa-{{ $social_footer['Name'] }}"></i></a>
                </li>
                @endforeach

            </ul>
        </div>
    </div>
    <div class="footer_copyright">
        <div class="container">
            <div class="pull-right">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#about">à propos</a></li>
                    <li><a href="#skill">compétences</a></li>
                    <li><a href="#education">parcours</a></li>
                    <!-- <li><a href="#service">SERVICES</a></li> -->
                    <li><a href="#portfolio">portfolio</a></li>
                    <li><a href="#contact">contact</a></li>
                </ul>
            </div>
            <div class="pull-left">
                <h5>
                    <!-- Licence -->
                    <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
                </h5>
            </div>
            <!--<div class="pull-left">
                <h5>
                    <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">antoinethys.com</span> est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.
                </h5>
            </div>-->
        </div>
    </div>
</footer>
<!--================End footer Area =================-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- Extra plugin js -->
<script src="{{ asset('vendors/counter-up/waypoints.min.js') }}"></script>
<script src="{{ asset('vendors/counter-up/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('vendors/isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('vendors/owl-carousel/owl.carousel.min.js') }}"></script>

<!--<script src="{{ asset('vendors/style-switcher/styleswitcher.js') }}"></script>-->
<!--<script src="{{ asset('vendors/style-switcher/switcher-active.js') }}"></script>-->
<script src="{{ asset('vendors/animate-css/wow.min.js') }}"></script>

<!-- Scroll and Progress -->
<script src="{{ asset('js/theme.js') }}"></script>

<!-- contact js -->
<script src="{{ asset('js/jquery.form.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/contact.js') }}"></script>

</body>
</html>
