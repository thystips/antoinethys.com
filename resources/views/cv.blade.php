@extends('layouts.cv')

@section('cv')
<div class="container main_container">
    <div class="content_inner_bg row m0">
        <section class="about_person_area pad" id="about">
            <div class="row">
                <div class="col-md-5">
                    <div class="person_img">
                        <img src="{{ asset('img/pdp.png') }}" alt="Photo de profil">
                        <a class="download_btn" href="{{ asset('storage/cv_antoine_thys.pdf') }}"><span>Télécharger mon CV</span></a>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row person_details">
                        <h3>Je suis <span>Antoine Thys</span></h3>
                        <h4>Étudiant Ynov Informatique</h4>
                        <p>Je m'appelle Antoine THYS. Je suis actuellement étudiant Ynov Infomatique sur le campus de Bordeaux. Je prépare un Mastère Expert Cloud, Sécurité &
                            Infrastructure en 5 ans. En plus d'être informaticien à plein temps je suis équipier
                            secouriste de la protection civile sur mon temps libre. Ce CV Web résulte d'un projet
                            obligatoire de première année de bachelor demandant la réalisation d'un CV sous forme de
                            site web dynamique avec une interface d'administration.</p>
                        <div class="person_information">
                            <ul>
                                <li><a href="#">Age</a></li>
                                <li><a href="#">Nationalité</a></li>
                                <li><a href="#">Téléphone</a></li>
                                <li><a href="#">Email</a></li>
                                <li><a href="#">Email Étudiante</a></li>
                                <li><a href="#">Site Web</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">21</a></li>
                                <li><a href="#">Français / Belge</a></li>
                                <li><a href="tel:+33640235596">+33 6 40 23 55 96</a></li>
                                <li><a href="mailto:contact@antoinethys.com?subject=Email from antoinethys.com">contact@antoinethys.com</a></li>
                                <li><a href="mailto:antoine.thys@ynov.com?subject=Email from antoinethys.com">antoine.thys@ynov.com</a></li>
                                <li><a href="https://antoinethys.com">antoinethys.com</a></li>
                            </ul>
                        </div>
                        <ul class="social_icon">
                            @foreach ($social as $social_ap)
                            <li style="">
                                <a href="https://{{ $social_ap['Link'] }}"><i class="fa fa-{{ $social_ap['Name'] }}"></i></a>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="myskill_area pad" id="skill">
            <div class="main_title">
                <h2>Mes compétences</h2>
            </div>
            <div class="row">
                <div class="col-md-6 wow fadeInUp animated">
                    <div class="skill_text">
                        <h4>Développement</h4>
                        <p>Mes compétences en developpement logiciel et web.</p>
                    </div>
                    <div class="skill_item_inner">
                        @foreach ($competences_dev as $comp)
                        <div class="single_skill">
                            <h4>{{ $comp['Name'] }}</h4>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ $comp['LevelPercent'] }}" aria-valuemin="0" aria-valuemax="100">
                                    <div class="progress_parcent">{{ $comp['LevelName'] }}</div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6 wow fadeInUp animated">
                    <div class="skill_text">
                        <h4>Infrastructure & SI</h4>
                        <p>Mes compétences en administration systèmes et réseaux.</p>
                    </div>
                    <div class="skill_item_inner">
                        @foreach ($competences_infra as $comp)
                        <div class="single_skill">
                            <h4>{{ $comp['Name'] }}</h4>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ $comp['LevelPercent'] }}" aria-valuemin="0" aria-valuemax="100">
                                    <div class="progress_parcent">{{ $comp['LevelName'] }}</div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="education_area pad" id="education">
            <div class="main_title">
                <h2>Parcours</h2>
            </div>
            <div class="education_inner_area">
                @foreach ($experiences as $exp)
                <div class="education_item wow fadeInUp animated" data-line="{{ $exp['Letter'] }}">
                    <h6>{{ $exp['Date'] }}</h6>
                    <a href="{{ $exp['Link'] }}"><h4>{{ $exp['Name'] }}</h4></a>
                    <h5>{{ $exp['Place'] }}</h5>
                    <p>{{ $exp['Description'] }}</p>
                </div>
                @endforeach
            </div>
        </section>
        <!--<section class="service_area" id="service">
            <div class="main_title">
                <h2>SERVICES</h2>
            </div>
            <div class="service_inner row">
                <div class="col-md-6">
                    <div class="service_item wow fadeInUp animated">
                        <i class="fa fa-wordpress" aria-hidden="true"></i>
                        <a href="#"><h4>Wordpress Development</h4></a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elits, sed do mod, tempor ets incididunt
                            ut labore et dolore magna aliqua. Ut enim adtiesm minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service_item wow fadeInUp animated">
                        <i class="fa fa-paint-brush" aria-hidden="true"></i>
                        <a href="#"><h4>Creative Design</h4></a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elits, sed do mod, tempor ets incididunt
                            ut labore et dolore magna aliqua. Ut enim adtiesm minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service_item wow fadeInUp animated">
                        <i class="fa fa-mobile" aria-hidden="true"></i>
                        <a href="#"><h4>Mobile Apps Development</h4></a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elits, sed do mod, tempor ets incididunt
                            ut labore et dolore magna aliqua. Ut enim adtiesm minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service_item wow fadeInUp animated">
                        <i class="fa fa-maxcdn" aria-hidden="true"></i>
                        <a href="#"><h4>Social Media Marketing</h4></a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elits, sed do mod, tempor ets incididunt
                            ut labore et dolore magna aliqua. Ut enim adtiesm minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service_item wow fadeInUp animated">
                        <i class="fa fa-camera" aria-hidden="true"></i>
                        <a href="#"><h4>Professional Photography</h4></a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elits, sed do mod, tempor ets incididunt
                            ut labore et dolore magna aliqua. Ut enim adtiesm minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service_item wow fadeInUp animated">
                        <i class="fa fa-laptop" aria-hidden="true"></i>
                        <a href="#"><h4>Website Development</h4></a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elits, sed do mod, tempor ets incididunt
                            ut labore et dolore magna aliqua. Ut enim adtiesm minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
            </div>
        </section>-->
        <section class="portfolio_area pad" id="portfolio">
            <div class="main_title">
                <h2>Portfolio</h2>
            </div>
            <div class="porfolio_menu">
                <ul class="causes_filter">
                    <li class="active" data-filter="*"><a href="">Tout</a></li>
                    @foreach ($portfolio_cat as $cat)
                    <li data-filter=".{{ $cat['Class'] }}"><a href="">{{ $cat['Name'] }}</a></li>
                    @endforeach

                </ul>
            </div>
            <div class="row">
                <div class="portfolio_list_inner">
                    @foreach ($portfolio as $projet)
                    <!--<div class="col-md-4 @foreach($projet['categories'] as $cat){{$cat['Class']}} @endforeach">-->
                    <div class="col-md-4 {{ $projet->categorie_slave['Class'] }}">
                        <div class="portfolio_item">
                            <div class="portfolio_img">
                                <a href="{{ $projet['Link'] }}" title="{{ $projet['Name'] }}"><img src="{{ asset($projet['Image']) }}" alt="Image-{{ $projet['Name'] }}"></a>
                            </div>
                            <div class="portfolio_title">
                                <a href="{{ $projet['Link'] }}"><h4>{{ $projet['Name'] }}</h4></a>
                                <h5>{{ $projet['Description'] }}</h5>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!--<section class="news_area pad" id="news">
            <div class="main_title">
                <h2>News</h2>
            </div>
            <div class="news_inner_area">
                <div class="row">
                    <div class="col-md-4">
                        <div class="twitter_area wow fadeInLeft animated">
                            <div class="w_title">
                                <h3><i class="fa fa-twitter"></i>Twitter</h3>
                            </div>
                            <ul>
                                <li><a href="#">@themexart:</a> Lorem ipsum dolors sit amets consectetur adipicing elit
                                    sed do eiusmod tempor incididunt ut labore.
                                </li>
                                <li><a href="#">@themexart:</a> Lorem ipsum dolors sit amets consectetur adipicing elit
                                    sed do eiusmod tempor incididunt ut labore.
                                </li>
                                <li><a href="#">@themexart:</a> Lorem ipsum dolors sit amets consectetur adipicing elit
                                    sed do eiusmod tempor incididunt ut labore.
                                </li>
                                <li><a href="#">@themexart:</a> Lorem ipsum dolors sit amets consectetur adipicing elit
                                    sed do eiusmod tempor incididunt ut labore.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="blog_slider_area wow fadeInUp animated">
                            <div class="w_title">
                                <h3>Blog</h3>
                            </div>
                            <div class="blog_slider_inner">
                                <div class="item">
                                    <img src="img/blog/blog-1.jpg" alt="">
                                    <a href="#"><h3>The Importance of User Experience</h3></a>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis... <a
                                            href="#">Read More</a></p>
                                    <h5>Posted by <a href="http://rocky.obaidul.com">Rocky</a> at 04 Feb, 2017</h5>
                                </div>
                                <div class="item">
                                    <img src="img/blog/blog-2.jpg" alt="">
                                    <a href="#"><h3>The Importance of User Experience</h3></a>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis... <a
                                            href="#">Read More</a></p>
                                    <h5>Posted by <a href="http://rocky.obaidul.com">Rocky</a> at 04 Feb, 2017</h5>
                                </div>
                                <div class="item">
                                    <img src="img/blog/blog-3.jpg" alt="">
                                    <a href="#"><h3>The Importance of User Experience</h3></a>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis... <a
                                            href="#">Read More</a></p>
                                    <h5>Posted by <a href="http://rocky.obaidul.com">Rocky</a> at 04 Feb, 2017</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="instagram_area wow fadeInRight animated">
                            <div class="w_title">
                                <h3><i class="fa fa-instagram"></i>Instagram</h3>
                            </div>
                            <ul>
                                <li><a href="#"><img src="img/instagram/instagram-1.jpg" alt=""></a></li>
                                <li><a href="#"><img src="img/instagram/instagram-2.jpg" alt=""></a></li>
                                <li><a href="#"><img src="img/instagram/instagram-3.jpg" alt=""></a></li>
                                <li><a href="#"><img src="img/instagram/instagram-4.jpg" alt=""></a></li>
                                <li><a href="#"><img src="img/instagram/instagram-5.jpg" alt=""></a></li>
                                <li><a href="#"><img src="img/instagram/instagram-6.jpg" alt=""></a></li>
                                <li><a href="#"><img src="img/instagram/instagram-7.jpg" alt=""></a></li>
                                <li><a href="#"><img src="img/instagram/instagram-8.jpg" alt=""></a></li>
                                <li><a href="#"><img src="img/instagram/instagram-9.jpg" alt=""></a></li>
                            </ul>
                            <a class="follow_btn" href="#"><i class="fa fa-instagram"></i> Follow on Instagram</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        <section class="contact_area pad" id="contact">
            <div class="main_title">
                <h2>Contact Us</h2>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="left_contact_details wow fadeInUp animated">
                        <div class="contact_title">
                            <h3>Informations de contact</h3>
                        </div>
                        <p>Pour tout renseignement je suis joignable via l'adresse email et le numéro de téléphone indiqués ce dessous.</p>
                        <p>Vous pouvez également me contacter sur mon adresse étudiant : antoine.thys@ynov.com si nécessaire.</p>
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="media-body">
                                <h4>Téléphone</h4>
                                <p><a href="tel:+33640235596">+33 6 40 23 55 96</a> </p>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <div class="media-body">
                                <h4>Email</h4>
                                <p><a href="mailto:contact@antoinethys.com?subject=email from antoinethys.com">contact@antoinethys.comw</a></p>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-key"></i>
                            </div>
                            <div class="media-body">
                                <h4>GPG</h4>
                                <p><a href="https://keybase.io/antoinethys/pgp_keys.asc">D51634831791296E</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact_from_area wow fadeInUp animated">
                        <div class="contact_title">
                            <h3>Envoyer moi un message</h3>
                        </div>
                        <div class="row">
                            <form action="{{url('sendemail/send')}}" method="post" id="contactForm">
                                {{ csrf_field() }}
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="Prénom*">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" name="last" id="last"
                                           placeholder="Nom*">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="email" class="form-control" name="email" id="email"
                                           placeholder="Email*">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" name="subject" id="subject"
                                           placeholder="Sujet*">
                                </div>
                                <div class="form-group col-md-12">
                                    <textarea class="form-control" rows="1" id="message" name="message"
                                              placeholder="Message*"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <button class="btn btn-default contact_btn" type="submit"><span>Envoyer le message</span>
                                    </button>
                                </div>
                            </form>
                            <div id="success">
                                <p>Votre message a été envoyé avec succès!</p>
                            </div>
                            <div id="error">
                                <p>Désolé un problème est survenu ! Votre message n'a pas été envoyé!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div style="height: 1px;padding: 3%"></div>
    </div>
</div>
@endsection
